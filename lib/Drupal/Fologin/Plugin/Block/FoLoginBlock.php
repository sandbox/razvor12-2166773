<?php


/**
 * @file
 * Contains \Drupal\FoLogin\Plugin\Block\FoLoginBlock.
 */



namespace Drupal\FoLogin\Plugin\Block;

use Drupal\block\Annotation\Block;
use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config;

/**
 * Provides a 'FoLogin: LoginBlock' block.
 *
 * @Block(
 *   id = "FoLoginBlock",
 *   subject = @Translation("FoLogin: LoginBlock"),
 *   admin_label = @Translation("FoLogin: LoginBlock")
 * )
 */
class FoLoginBlock extends BlockBase {

  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {
    global $base_url;

    //Generate url to login
    return array(
        '#type' => 'markup',
        '#markup' => '<a href="'.$base_url.'/fblogin?redirect=1">Login with Facebook</a>'
      );
  }

}
