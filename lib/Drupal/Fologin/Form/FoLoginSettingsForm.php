<?php
 
/**
 * @file
 * Contains \Drupal\Fologin\Form\FologinSettingsForm
 */
 
namespace Drupal\FoLogin\Form;
 
use Drupal\Core\Form\ConfigFormBase;
 
class FologinSettingsForm extends ConfigFormBase{
  
  public function getFormId()
  {
    return 'fologin_configure';
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, array &$form_state)
  {
    $config = $this->configFactory->get('FoLogin.settings');

    $form['FoLogin'] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings'),
    );
    
    $form['FoLogin']['facebook_client_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Facebook client ID'),
      '#default_value' => $config->get('facebook_client_id'),
    );
    
    
    $form['FoLogin']['facebook_client_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Facebook client secret'),
      '#default_value' => $config->get('facebook_client_secret'),
    );
    
    
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, array &$form_state)
  {
    $this
    ->configFactory
    ->get('FoLogin.settings')
    ->set('facebook_client_id', $form_state['values']['facebook_client_id'])
    ->set('facebook_client_secret', $form_state['values']['facebook_client_secret'])
    ->save();
    
    parent::submitForm($form, $form_state); 
  }
  
}