<?php

  
 
namespace Drupal\FoLogin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\user\RegisterFormController;
use Drupal\user;


 
class FoLoginController extends ControllerBase{
 
  /*
  * Function for save new users from Facebook
  */  

  public function saveUser($details) {
    
    //fields for database
    $fields = array(
         'name' =>    $details->name,
         'status' =>  1,
         'created' => time(),
         'pass' =>    $details->id
       );

    
    //create new user
    $account = \Drupal::entityManager()
            ->getStorageController("user")
            ->create($fields);

    //save created user
    $account->save();

    //update facebook id for user. Write in pass field
    $update_fbid = db_update('users')->fields(array('pass' => $details->id))->condition('uid', $account->id(), '=')->execute();
    

    return $account;
  }

  /*
  * Function check exists user
  */
  public function existsUser($fbid){

    //sql request for users
    $sql = "SELECT pass FROM users WHERE pass = :fbid";

    //query to db and set virble $fbid 
    $result = db_query($sql, array(
      ':fbid' => $fbid,
    ));

    $strings = $result->fetchAll();
    
    //return count of users 0 or 1 (true or false)
    return (bool) count($strings);
  }

  /*
  * Get user by fbid
  */
  public function selectUserByFBid($field, $fbid){
    
    //sql request for db
    $sql = "SELECT * FROM users WHERE pass = :fbid LIMIT 1";
    $result = db_query($sql, array(':fbid' => $fbid))->fetchAssoc();
    
    //get rows from request
    return $result[$field];
  }

  /*
  * Create Page 
  */
  public function FoLoginPage() {
    
    global $user;
    global $base_url;

    //parsing configuration file
    $config=$this->config('FoLogin.settings');

    //array with redirect parmas
    $par=array(
      'url' =>       $base_url,
      'app_id' =>    $config->get('facebook_client_id'),
      'app_secret' => $config->get('facebook_client_secret')
    );    

    //If facebook return code
    if(isset( $_GET['code'])) {
        
        //Request for access token
        $cnt= file_get_contents('https://graph.facebook.com/oauth/access_token?client_id='.$par['app_id'].'&redirect_uri='.$par['url'].'/fblogin&client_secret='.$par['app_secret'].'&code='.$_GET['code']);
        
        //Parsing url for access token
        $access_token=explode("&", $cnt);
        $access_token=explode("=", $access_token[0]);
        $access_token=$access_token[1];

        //reuest for user params and parsing JSON
        $fb_query_result=json_decode(file_get_contents('https://graph.facebook.com/me?access_token='.$access_token));

        //Check for error
        if(isset($fb_query_result->error)){
          
          //Return error message if error has detected 
          $build=array('#markup'=>t('Error to login with Facebook. Try again later'));
        
        }else{
          //check for logined user alredy exists
          if($this->existsUser($fb_query_result->id)){

            //if exists load user by id
            $svu=user_load($this->selectUserByFBid('uid', $fb_query_result->id));

          }else{

            //if not exists create new user
            $svu=$this->saveUser($fb_query_result);
            
          }

          //Login new user 
          user_login_finalize($svu);

          //Show message and redirect to homepage
          $build=array('#markup' =>'Welcome to '.$base_url.','.$fb_query_result->{"name"}.'<meta http-equiv="refresh" content="0; url='.$base_url.'" />');

        }

    }else{
      if($_GET['redirect']){

        //Request for code 
        $build=array('#markup' => '<meta http-equiv="refresh" content="0; url=https://www.facebook.com/dialog/oauth?client_id='.$par['app_id'].'&redirect_uri='.$par['url'].'/fblogin&response_type=code" />');
      }else{
        $build=array('#markup' => t(''));
      }
      
    }

    return $build;
  }
 
} 