+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			FoLogin - Authorization via FaceBook
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

FoLogin-module for Drupal 8.x for authorization via Facebook.

Requirements
------------
Drupal 8.x

Installation
------------

All installation process you can find in screencast: https://www.youtube.com/watch?v=NGhIWxkr194

------------
1. In order for your site to use Facebook Connect you must register an
   application on Facebook. Visit https://www.facebook.com/developers/appså
   and create a new application, usually simply the name of your website such as
   "example.com".

2. While setting up your application, set the "Deauthorize Callback" to
   "http://example.com/fboauth/deauthorize". This will allow Facebook OAuth to
   cleanup user information if the application is disconnected from the Facebook
   site.

3. Copy the entire fboauth directory the Drupal /modules directory.

4. Login as an administrator. Enable the module on the Modules page.

5. Configure the Facebook OAuth module settings under "Configuration" ->
   "Services" -> "Facebook autendification settings". Copy and paste the App ID and
   App Secret from your newly created Facebook application's settings.

   Note that it is highly recommended to request access to the Facebook user's
   e-mail address so that normal Drupal functionality (the password reset for
   example) will continue to work even if the user has logged in with Facebook.
   This option is enabled by default.

   If you have installed the Profile module, you may also map information
   between your Profile fields and Facebook's available fields.

6. Enable module in structure->Block Laout. Click FOLOGIN->FoLogin: LoginBlock and click "Save block". Drop block to   
   desired location.


